package me.haslett.collections;

public class QueueNode<T> {

	public T value;
	public QueueNode<T> nextNode;

	public QueueNode( T value, QueueNode<T> nextNode ) {
		this.value = value;
		this.nextNode = nextNode;
	}

}
