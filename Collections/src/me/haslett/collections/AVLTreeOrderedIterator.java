package me.haslett.collections;

/**
 * Iterates an AVL tree in natural order represented by the Key.  This iterator traverses the AVLTree in the natural order as determined by comparing TKey.
 * 
 * @author Jim Haslett
 * @since Mar 8, 2013
 * @param <TKey>	Generic type representing the key used for sorting.  Must be Comparable.
 * @param <TValue>	Generic type representing the data being stored.
 */
public class AVLTreeOrderedIterator<TKey extends Comparable<TKey>, TValue> extends AVLTreeIterator<TKey, TValue> {

	/**
	 * Constructor.
	 * 
	 * @param Root AVLTreeNode where the iteration will start.
	 * @throws Exception
	 */
	public AVLTreeOrderedIterator( AVLTreeNode<TKey, TValue> Root ) throws Exception {
		super( Root );
	}

    protected boolean MoveNext() throws Exception
    {
        if( _status == StatusEnum.Invalid )
        {
            throw ( new Exception( "! AVL Tree has changed, this Iterator is no longer valid !" ) );
        }
        else if( _status == StatusEnum.BeforeFirst )
        {
            if( _root != null )
            {
                _stack.clear();
                _stack.push( null );
                _stack.push( _root );
                StackLeftToNull();
                _status = StatusEnum.OK;
            }
            else
            {
                _status = StatusEnum.AfterLast;
                _stack.clear();
                _current = null;
                return false;
            }
        }
        else if( _status == StatusEnum.AfterLast )
        {
            _current = null;
            _stack.clear();
            return false;
        }
        else if( _current.getRight() != null )
        {
            _stack.push( _current.getRight() );
            StackLeftToNull();
        }

        _current = _stack.pop();
        if( _current == null )
        {
            _status = StatusEnum.AfterLast;
            _stack.clear();
            return false;
        }
        return true;
    }

	
    protected void StackLeftToNull() throws Exception
    {
        AVLTreeNode<TKey, TValue> current = _stack.peek();
        if( current == null )
            throw ( new Exception( "! Internal error: Invalid stack operation !" ) );
        else
        {
            current = current.getLeft();
            while( current != null )
            {
                _stack.push( current );
                current = current.getLeft();
            }
        }
    }

}
