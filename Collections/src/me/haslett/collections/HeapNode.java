package me.haslett.collections;

public class HeapNode<TWeight extends Comparable<TWeight>, TValue> {

	protected TValue _value;
	protected TWeight _weight;

	public HeapNode( TWeight weight, TValue value ) {
		_weight = weight;
		_value = value;
	}

	public TWeight getWeight() {
		return _weight;
	}

	public TValue getValue() {
		return _value;
	}
	
	@Override
	public String toString() {
		return _value + "(" + _weight + ")";
	}

}
