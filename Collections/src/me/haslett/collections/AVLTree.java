package me.haslett.collections;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * AVL Balanced Binary Search Tree.
 * 
 * An AVL tree is a self-balancing binary search tree. In an AVL tree, the
 * heights of the two child subtrees of any node differ by at most one; if at
 * any time they differ by more than one, rebalancing is done to restore this
 * property. Lookup, insertion, and deletion all take O(log n) time in both the
 * average and worst cases, where n is the number of nodes in the tree prior to
 * the operation. Insertions and deletions may require the tree to be rebalanced
 * by one or more tree rotations.
 * 
 * @author Jim Haslett
 * @since Mar 8, 2013
 * @param <TKey>
 *            Generic type representing the key used for sorting. Must be
 *            Comparable.
 * @param <TValue>
 *            Generic type representing the data being stored.
 */
public class AVLTree<TKey extends Comparable<TKey>, TValue> implements Iterable<MapEntry<TKey, TValue>> {

	/**
	 * Creates a new AVLTree specifying the traversal method of iteration.
	 * 
	 * @param TraversalMethod
	 *            Defines the traversal method used in iteration.
	 */
	public AVLTree( AVLTreeTraversalMethod TraversalMethod ) {
		_root = null;
		_count = 0;
		_traversalMethod = TraversalMethod;
	}

	/**
	 * Creates a new AVLTree that defaults to InOrder traversal.
	 */
	public AVLTree() {
		this( AVLTreeTraversalMethod.InOrder );
	}

	protected AVLTreeNode<TKey, TValue> _root;
	protected int _count;
	protected AVLTreeTraversalMethod _traversalMethod;

	/**
	 * Returns the number of elements in the tree.
	 * 
	 * @return Number of elements in the tree.
	 */
	public int getCount() {
		return _count;
	}

	/**
	 * Returns the current traversal method used for iteration.
	 * 
	 * @return Current traversal method for iteration.
	 */
	public AVLTreeTraversalMethod getTraversalMethod() {
		return _traversalMethod;
	}

	/**
	 * Set the traversal method for iteration.
	 * 
	 * @param traversalMethod
	 *            New traversal method.
	 */
	public void setTraversalMethod( AVLTreeTraversalMethod traversalMethod ) {
		_traversalMethod = traversalMethod;
	}

	/**
	 * Gets a MapEntry representing they key/value pair indexed by key. This is
	 * the equivalent of an array indexer.
	 * 
	 * @param Key
	 *            Key to locate in the tree.
	 * @return MapEntry representing the key/value pair found. Null if key is
	 *         not found.
	 */
	public MapEntry<TKey, TValue> get( TKey Key ) {

		AVLTreeNode<TKey, TValue> current = _root;
		int result = 0;
		if ( current != null )
			result = current.getKey().compareTo( Key );

		while ( result != 0 && current != null ) {
			if ( result < 0 ) // current.key < key go right
			{
				current = current.getRight();
			} else // result > 0 current.key > key go left
			{
				current = current.getLeft();
			}
			if ( current != null )
				result = current.getKey().compareTo( Key );
		}

		return ( current == null ) ? null : current.getMapEntry();
	}

	/**
	 * Returns the key with the minimum value.
	 * 
	 * @return Minimum valued key in the tree.
	 */
	public TKey getMinKey() {

		if ( _root == null )
			return null;
		AVLTreeNode<TKey, TValue> current = _root;
		while ( current.getLeft() != null )
			current = current.getLeft();
		return current.getKey();
	}

	/**
	 * Returns the key with the maximum value.
	 * 
	 * @return Maximum valued key in the tree.
	 */
	public TKey getMaxKey() {

		if ( _root == null )
			return null;
		AVLTreeNode<TKey, TValue> current = _root;
		while ( current.getRight() != null )
			current = current.getRight();
		return current.getKey();
	}

	@Override
	public Iterator<MapEntry<TKey, TValue>> iterator() {
		try {
			if ( _traversalMethod == AVLTreeTraversalMethod.InOrder )
				return new AVLTreeOrderedIterator<TKey, TValue>( _root );
			else if ( _traversalMethod == AVLTreeTraversalMethod.ReverseOrder )
				return new AVLTreeReverseOrderIterator<TKey, TValue>( _root );
			else if ( _traversalMethod == AVLTreeTraversalMethod.TopDown )
				return new AVLTreeTopDownOrderIterator<TKey, TValue>( _root );
			else
				return new AVLTreeOrderedIterator<TKey, TValue>( _root );
		} catch ( Exception e ) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Clear the contents of the tree.
	 */
	public void Clear() {
		_root = null;
		_count = 0;
	}

	/**
	 * Add a key/value pair to the tree.
	 * 
	 * @param Key
	 *            Key used for ordering the tree entries.
	 * @param Value
	 *            Value to be stored.
	 * @throws Exception
	 */
	public void Add( TKey Key, TValue Value ) throws Exception {
		Stack<AVLTreeNode<TKey, TValue>> stack = new Stack<AVLTreeNode<TKey, TValue>>();

		AVLTreeNode<TKey, TValue> node = new AVLTreeNode<TKey, TValue>( Key, Value );

		AVLTreeNode<TKey, TValue> current = _root;
		AVLTreeNode<TKey, TValue> parent = null;

		stack.push( null );

		int result;

		while ( current != null ) {
			result = current.getKey().compareTo( node.getKey() );

			stack.push( current );

			if ( result == 0 ) {
				// Duplicate Value, throw exception
				throw ( new Exception( "! Key already exists in Tree !" ) );
			} else if ( result < 0 ) // current.key < node.key
			{
				parent = current;
				current = current.getRight();
			} else // result > 0 current.key > node.key
			{
				parent = current;
				current = current.getLeft();
			}
		}

		_count++;

		if ( parent == null ) // Empty Tree
			_root = node;
		else {
			result = parent.getKey().compareTo( node.getKey() );
			if ( result < 0 ) // parent.key < node.key add to right
			{
				parent.setRight( node );
			} else // parent.key > node.key add to left
			{
				parent.setLeft( node );
			}
		}

		// Go back up the tree and reset height
		current = stack.pop();
		while ( current != null ) {
			current.CalculateHeight();
			if ( current.getBalanceFactor() > 1 ) {
				if ( current.getLeft().getBalanceFactor() < 0 )
					RotateLeft( current.getLeft(), current );
				RotateRight( current, stack.peek() );
			} else if ( current.getBalanceFactor() < -1 ) {
				if ( current.getRight().getBalanceFactor() > 0 )
					RotateRight( current.getRight(), current );
				RotateLeft( current, stack.peek() );
			}
			current = stack.pop();
		}
	}

	/**
	 * Remove an entry from the tree.
	 * 
	 * @param Key
	 *            Key of entry to remove.
	 * @return MapEntry representing the key/value pair that was removed.
	 */
	public MapEntry<TKey, TValue> Remove( TKey Key ) {
		Stack<AVLTreeNode<TKey, TValue>> stack = new Stack<AVLTreeNode<TKey, TValue>>();

		AVLTreeNode<TKey, TValue> removed = null;

		AVLTreeNode<TKey, TValue> current = _root;
		AVLTreeNode<TKey, TValue> parent = null;

		stack.push( null );

		int result = 0;
		if ( current != null )
			result = current.getKey().compareTo( Key );

		while ( result != 0 && current != null ) {
			stack.push( current );

			if ( result < 0 ) // current.key < key
			{
				parent = current;
				current = current.getRight();
			} else // result > 0 current.key > key
			{
				parent = current;
				current = current.getLeft();
			}
			if ( current != null )
				result = current.getKey().compareTo( Key );
		}

		if ( current == null ) // Key not found, throw exception
		{
			// throw( new ApplicationException(
			// "! Key not found in tree, nothing removed !" ) );
			return null;
		} else {
			_count--;
			removed = current;

			/*
			 * Case 1: If the node being deleted has no right child, then the
			 * node's left child can be used as the replacement. The binary
			 * search tree property is maintained because we know that the
			 * deleted node's left subtree itself maintains the binary search
			 * tree property, and that the values in the left subtree are all
			 * less than or all greater than the deleted node's parent,
			 * depending on whether the deleted node is a left or right child.
			 * Therefore, replacing the deleted node with its left subtree
			 * maintains the binary search tree property.
			 */
			if ( current.getRight() == null ) {
				if ( current.getLeft() != null )
					stack.push( current.getLeft() );
				if ( parent == null ) // deleting the root
					_root = current.getLeft();
				else {
					result = parent.getKey().compareTo( current.getKey() );
					if ( result < 0 ) // parent.key < current.key, current is
										// parent.right
					{
						parent.setRight( current.getLeft() );
					} else // result > 0 parent.key > current.key, current is
							// parent.left
					{
						parent.setLeft( current.getLeft() );
					}
				}
			}
			/*
			 * Case 2: If the deleted node's right child has no left child, then
			 * the deleted node's right child can replace the deleted node. The
			 * binary search tree property is maintained because the deleted
			 * node's right child is greater than all nodes in the deleted
			 * node's left subtree and is either greater than or less than the
			 * deleted node's parent, depending on whether the deleted node was
			 * a right or left child. Therefore, replacing the deleted node with
			 * its right child maintains the binary search tree property.
			 */
			else if ( current.getRight().getLeft() == null ) {
				stack.push( current.getRight() );
				current.getRight().setLeft( current.getLeft() );
				if ( parent == null ) // deleting the root
					_root = current.getRight();
				else {
					result = parent.getKey().compareTo( current.getKey() );
					if ( result < 0 ) // parent.key < current.key, current is
										// parent.right
					{
						parent.setRight( current.getRight() );
					} else // result > 0 parent.key > current.key, current is
							// parent.left
					{
						parent.setLeft( current.getRight() );
					}
				}
			}
			/*
			 * Case 3: Finally, if the deleted node's right child does have a
			 * left child, then the deleted node needs to be replaced by the
			 * deleted node's right child's left-most descendant. That is, we
			 * replace the deleted node with the right subtree's smallest value.
			 */
			else {
				AVLTreeNode<TKey, TValue> lmparent = current.getRight();
				AVLTreeNode<TKey, TValue> leftmost = lmparent.getLeft();

				Queue<AVLTreeNode<TKey, TValue>> lmqueue = new LinkedList<AVLTreeNode<TKey, TValue>>();

				lmqueue.add( lmparent );

				// Find the leftmost node of current's right node, and it's
				// parent.
				while ( leftmost.getLeft() != null ) {
					lmqueue.add( leftmost );
					lmparent = leftmost;
					leftmost = lmparent.getLeft();
				}

				// Set the leftmost's parent's left node to the leftmosts right
				// node
				lmparent.setLeft( leftmost.getRight() );

				// Set leftmost's left and right equal to current's left and
				// right
				leftmost.setRight( current.getRight() );
				leftmost.setLeft( current.getLeft() );

				if ( parent == null ) // deleting the root
					_root = leftmost;
				else {
					result = parent.getKey().compareTo( current.getKey() );
					if ( result < 0 ) // parent.key < current.key, current is
										// parent.right
					{
						parent.setRight( leftmost );
					} else // result > 0 parent.key > current.key, current is
							// parent.left
					{
						parent.setLeft( leftmost );
					}
				}
				stack.push( leftmost );
				while ( !lmqueue.isEmpty() )
				{
					stack.push( lmqueue.poll() );
				}
			}

			current = stack.pop();
			while ( current != null ) {
				current.CalculateHeight();
				if ( current.getBalanceFactor() > 1 ) {
					if ( current.getLeft().getBalanceFactor() < 0 )
						RotateLeft( current.getLeft(), current );
					RotateRight( current, stack.peek() );
				} else if ( current.getBalanceFactor() < -1 ) {
					if ( current.getRight().getBalanceFactor() > 0 )
						RotateRight( current.getRight(), current );
					RotateLeft( current, stack.peek() );
				}
				current = stack.pop();
			}

			return removed.getMapEntry();
		}
	}

	private void RotateRight( AVLTreeNode<TKey, TValue> Node, AVLTreeNode<TKey, TValue> Parent ) {
		AVLTreeNode<TKey, TValue> LeftNode = Node.getLeft();
		Node.setLeft( LeftNode.getRight() );
		LeftNode.setRight( Node );

		Node.CalculateHeight();
		LeftNode.CalculateHeight();

		if ( Parent == null )
			_root = LeftNode;
		else {
			if ( LeftNode.getKey().compareTo( Parent.getKey() ) < 0 )
				Parent.setLeft( LeftNode );
			else
				Parent.setRight( LeftNode );
		}
	}

	private void RotateLeft( AVLTreeNode<TKey, TValue> Node, AVLTreeNode<TKey, TValue> Parent ) {
		AVLTreeNode<TKey, TValue> RightNode = Node.getRight();
		Node.setRight( RightNode.getLeft() );
		RightNode.setLeft( Node );

		Node.CalculateHeight();
		RightNode.CalculateHeight();

		if ( Parent == null )
			_root = RightNode;
		else {
			if ( RightNode.getKey().compareTo( Parent.getKey() ) < 0 )
				Parent.setLeft( RightNode );
			else
				Parent.setRight( RightNode );
		}
	}

}
