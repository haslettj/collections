package me.haslett.collections;

import java.util.ArrayList;

public class Graph<TNodeKey extends Comparable<TNodeKey>, TNodeValue, TEdgeMeta> {

	protected AVLTree<TNodeKey, GraphNode<TNodeKey, TNodeValue, TEdgeMeta>> _nodes;

	public Graph() {
		_nodes = new AVLTree<TNodeKey, GraphNode<TNodeKey, TNodeValue, TEdgeMeta>>();
	}

	public void addNode( TNodeKey key, TNodeValue value ) throws Exception {
		_nodes.Add( key, new GraphNode<TNodeKey, TNodeValue, TEdgeMeta>( key, value ) );
	}

	public void deleteNode( TNodeKey key ) throws Exception {
		GraphNode<TNodeKey, TNodeValue, TEdgeMeta> nodeA;
		nodeA = _nodes.get( key ).getValue();
		if ( nodeA == null ) throw new Exception( "Node not found in Graph" );

		for ( GraphEdge<TNodeKey, TNodeValue, TEdgeMeta> e : nodeA.getEdges() ) {
			if ( e.getNodeA() == nodeA ) {
				e.getNodeB().removeEdge( e );
			} else {
				e.getNodeA().removeEdge( e );
			}
		}

		_nodes.Remove( nodeA.getKey() );

	}

	public void connectNodes( TNodeKey a, TNodeKey b, int direction, TEdgeMeta meta ) throws Exception {
		GraphNode<TNodeKey, TNodeValue, TEdgeMeta> nodeA, nodeB;
		nodeA = _nodes.get( a ).getValue();
		nodeB = _nodes.get( b ).getValue();

		if ( nodeA == null || nodeB == null ) throw new Exception( "Node not found in Graph" );

		if ( nodeA.adjacentTo( nodeB ) ) throw new Exception( "Nodes already adjacent to each other" );

		GraphEdge<TNodeKey, TNodeValue, TEdgeMeta> edge = new GraphEdge<TNodeKey, TNodeValue, TEdgeMeta>( nodeA, nodeB, direction, meta );
		nodeA.addEdge( edge );
		nodeB.addEdge( edge );

	}

	public void disconnectNodes( TNodeKey a, TNodeKey b ) throws Exception {
		GraphNode<TNodeKey, TNodeValue, TEdgeMeta> nodeA, nodeB;
		nodeA = _nodes.get( a ).getValue();
		nodeB = _nodes.get( b ).getValue();

		if ( nodeA == null || nodeB == null ) throw new Exception( "Node not found in Graph" );

		GraphEdge<TNodeKey, TNodeValue, TEdgeMeta> e = nodeA.getEdgeTo( nodeB );

		if ( e == null ) throw new Exception( "Nodes not adjacent to each other" );

		nodeA.removeEdge( e );
		nodeB.removeEdge( e );
	}

	public ArrayList<MapEntry<TNodeKey, TNodeValue>> getNeighbors( TNodeKey key ) throws Exception {
		GraphNode<TNodeKey, TNodeValue, TEdgeMeta> nodeA;
		nodeA = _nodes.get( key ).getValue();

		if ( nodeA == null ) throw new Exception( "Node not found in Graph" );

		ArrayList<MapEntry<TNodeKey, TNodeValue>> list = new ArrayList<MapEntry<TNodeKey, TNodeValue>>();

		for ( GraphNode<TNodeKey, TNodeValue, TEdgeMeta> n : nodeA.getAdjacentNodes() ) {
			list.add( new MapEntry<TNodeKey, TNodeValue>( n.getKey(), n.getValue() ) );
		}

		return list;
	}

	public TNodeValue getNodeValue( TNodeKey key ) throws Exception {
		GraphNode<TNodeKey, TNodeValue, TEdgeMeta> nodeA;
		nodeA = _nodes.get( key ).getValue();
		if ( nodeA == null ) throw new Exception( "Node not found in Graph" );

		return nodeA.getValue();
	}

	public void setNodeValue( TNodeKey key, TNodeValue value ) throws Exception {
		GraphNode<TNodeKey, TNodeValue, TEdgeMeta> nodeA;
		nodeA = _nodes.get( key ).getValue();
		if ( nodeA == null ) throw new Exception( "Node not found in Graph" );

		nodeA.setValue( value );
	}

	public boolean Adjacent( TNodeKey a, TNodeKey b ) throws Exception {
		GraphNode<TNodeKey, TNodeValue, TEdgeMeta> nodeA, nodeB;
		nodeA = _nodes.get( a ).getValue();
		nodeB = _nodes.get( b ).getValue();

		if ( nodeA == null || nodeB == null ) throw new Exception( "Node not found in Graph" );

		return nodeA.adjacentTo( nodeB );
	}

	public TEdgeMeta getEdgeMeta( TNodeKey a, TNodeKey b) throws Exception {
		
		GraphNode<TNodeKey, TNodeValue, TEdgeMeta> nodeA, nodeB;
		nodeA = _nodes.get( a ).getValue();
		nodeB = _nodes.get( b ).getValue();

		if ( nodeA == null || nodeB == null ) throw new Exception( "Node not found in Graph" );

		GraphEdge<TNodeKey, TNodeValue, TEdgeMeta> e = nodeA.getEdgeTo( nodeB );

		if ( e == null ) throw new Exception( "Nodes not adjacent to each other" );

		return e.getMeta();
	}

	public void setEdgeMeta( TNodeKey a, TNodeKey b, TEdgeMeta meta) throws Exception {
		GraphNode<TNodeKey, TNodeValue, TEdgeMeta> nodeA, nodeB;
		nodeA = _nodes.get( a ).getValue();
		nodeB = _nodes.get( b ).getValue();

		if ( nodeA == null || nodeB == null ) throw new Exception( "Node not found in Graph" );

		GraphEdge<TNodeKey, TNodeValue, TEdgeMeta> e = nodeA.getEdgeTo( nodeB );

		if ( e == null ) throw new Exception( "Nodes not adjacent to each other" );

		
		e.setMeta( meta );
	}

}
