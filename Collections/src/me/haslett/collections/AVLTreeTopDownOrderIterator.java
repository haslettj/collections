package me.haslett.collections;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Iterates an AVL tree in top down order. This iterator traverses the AVLTree
 * in special way that is useful for serializing or saving the tree for the
 * purpose of reloading another tree. Inserting the elements into a tree in the
 * order they are iterated here is the fastest way to load the tree without
 * necessary and costly sorting.
 * 
 * @author Jim Haslett
 * @since Mar 8, 2013
 * @param <TKey>
 *            Generic type representing the key used for sorting. Must be
 *            Comparable.
 * @param <TValue>
 *            Generic type representing the data being stored.
 */
public class AVLTreeTopDownOrderIterator<TKey extends Comparable<TKey>, TValue> extends AVLTreeIterator<TKey, TValue> {

	/**
	 * Constructor.
	 * 
	 * @param Root
	 *            AVLTreeNode where the iteration will start.
	 * @throws Exception
	 */
	public AVLTreeTopDownOrderIterator( AVLTreeNode<TKey, TValue> Root ) throws Exception {
		super( Root );
		// TODO Auto-generated constructor stub

		if ( lQueue == null ) {
			lQueue = new LinkedList<AVLTreeNode<TKey, TValue>>();
		}
	}

	protected Queue<AVLTreeNode<TKey, TValue>> lQueue;

	@Override
	protected boolean MoveNext() throws Exception {
		if ( lQueue == null ) {
			lQueue = new LinkedList<AVLTreeNode<TKey, TValue>>();
		}
		if ( _status == StatusEnum.Invalid ) {
			throw ( new Exception( "! AVL Tree has changed, this enumerator is no longer valid !" ) );
		} else if ( _status == StatusEnum.BeforeFirst ) {
			if ( _root != null ) {
				lQueue.clear();
				lQueue.add( _root );
				_status = StatusEnum.OK;
			} else {
				_status = StatusEnum.AfterLast;
				lQueue.clear();
				_current = null;
				return false;
			}
		} else if ( _status == StatusEnum.AfterLast ) {
			_current = null;
			lQueue.clear();
			return false;
		}

		if ( !lQueue.isEmpty() )// ( lQueue.size() > 0 )
		{
			_current = lQueue.poll();
			if ( _current.getLeft() != null )
				lQueue.add( _current.getLeft() );
			if ( _current.getRight() != null )
				lQueue.add( _current.getRight() );
		} else {
			_status = StatusEnum.AfterLast;
			lQueue.clear();
			return false;
		}
		return true;
	}

}
