package me.haslett.collections;

public class GraphEdge<TNodeKey, TNodeValue, TEdgeMeta> {

	public static final int DIRECTION_NONE = 0;
	public static final int DIRECTION_A_TO_B = 1;
	public static final int DIRECTION_B_TO_A = 2;
	public static final int DIRECTION_BOTH = 3;

	protected GraphNode<TNodeKey, TNodeValue, TEdgeMeta> _nodeA;
	protected GraphNode<TNodeKey, TNodeValue, TEdgeMeta> _nodeB;

	protected TEdgeMeta _meta;

	protected int _direction;

	public GraphEdge( GraphNode<TNodeKey, TNodeValue, TEdgeMeta> A, GraphNode<TNodeKey, TNodeValue, TEdgeMeta> B, int Direction, TEdgeMeta Meta ) {
		_nodeA = A;
		_nodeB = B;

		_direction = Direction;

		_meta = Meta;
	}

	public GraphNode<TNodeKey, TNodeValue, TEdgeMeta> getNodeA() {
		return _nodeA;
	}

	public GraphNode<TNodeKey, TNodeValue, TEdgeMeta> getNodeB() {
		return _nodeB;
	}

	public int getDirection() {
		return _direction;
	}

	public boolean connectsTo( GraphNode<TNodeKey, TNodeValue, TEdgeMeta> node ) {
		return ( node == _nodeA ) || ( node == _nodeB );
	}

	public TEdgeMeta getMeta() {
		return _meta;
	}

	public void setMeta( TEdgeMeta meta ) {
		_meta = meta;
	}
}
