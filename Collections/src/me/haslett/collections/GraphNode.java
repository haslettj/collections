package me.haslett.collections;

import java.util.ArrayList;

/**
 * Node used in an Graph.
 * 
 * @author Jim Haslett
 * @since December 14, 2014
 * @param <TNodeValue>
 *            Generic type representing the data being stored.
 */
public class GraphNode<TNodeKey, TNodeValue, TEdgeMeta> {
	protected ArrayList<GraphEdge<TNodeKey, TNodeValue, TEdgeMeta>> _edges;
	protected TNodeValue _value;
	protected TNodeKey _key;

	public GraphNode( TNodeKey key, TNodeValue value ) {
		_edges = new ArrayList<GraphEdge<TNodeKey, TNodeValue, TEdgeMeta>>();
		_value = value;
		_key = key;
	}

	public boolean addEdge( GraphEdge<TNodeKey, TNodeValue, TEdgeMeta> newEdge ) {
		return _edges.add( newEdge );
	}

	public GraphEdge<TNodeKey, TNodeValue, TEdgeMeta> removeEdge( int index ) {
		return _edges.remove( index );
	}

	public boolean removeEdge( GraphEdge<TNodeKey, TNodeValue, TEdgeMeta> edge ) {
		return _edges.remove( edge );
	}

	public ArrayList<GraphEdge<TNodeKey, TNodeValue, TEdgeMeta>> getEdges() {
		return _edges;
	}

	public ArrayList<GraphNode<TNodeKey, TNodeValue, TEdgeMeta>> getAdjacentNodes() {
		ArrayList<GraphNode<TNodeKey, TNodeValue, TEdgeMeta>> list = new ArrayList<GraphNode<TNodeKey, TNodeValue, TEdgeMeta>>();

		for ( GraphEdge<TNodeKey, TNodeValue, TEdgeMeta> e : _edges ) {
			if ( e.getNodeA() == this ) {
				list.add( e.getNodeB() );
			} else if ( e.getNodeB() == this ) {
				list.add( e.getNodeA() );
			}
		}

		return list;
	}

	public boolean adjacentTo( GraphNode<TNodeKey, TNodeValue, TEdgeMeta> node ) {

		boolean ret = false;

		for ( GraphEdge<TNodeKey, TNodeValue, TEdgeMeta> e : _edges ) {
			if ( e.connectsTo( node ) ) {
				ret = true;
				break;
			}
		}
		return ret;
	}

	public GraphEdge<TNodeKey, TNodeValue, TEdgeMeta> getEdgeTo( GraphNode<TNodeKey, TNodeValue, TEdgeMeta> node ) {

		for ( GraphEdge<TNodeKey, TNodeValue, TEdgeMeta> e : _edges ) {
			if ( e.connectsTo( node ) ) { return e; }
		}
		return null;
	}

	public TNodeKey getKey() {
		return _key;
	}

	public TNodeValue getValue() {
		return _value;
	}

	public void setValue( TNodeValue value ) {
		_value = value;
	}
}
