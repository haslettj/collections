package me.haslett.collections;

import java.util.ArrayList;

import javax.xml.crypto.NodeSetData;

public class MinHeap<TWeight extends Comparable<TWeight>, TValue> {

	protected ArrayList<HeapNode<TWeight, TValue>> _nodes;

	public MinHeap() {
		_nodes = new ArrayList<HeapNode<TWeight, TValue>>();

		_nodes.add( 0, null );
	}

	public void add( TWeight weight, TValue value ) {
		int i = _nodes.size();

		_nodes.add( i, new HeapNode<TWeight, TValue>( weight, value ) );

		if ( i > 1 ) {
			while ( _nodes.get( i ).getWeight().compareTo( _nodes.get( PARENT( i ) ).getWeight() ) < 0 ) { // if
																											// smaller
																											// than
																											// parent
				HeapNode<TWeight, TValue> temp = _nodes.get( i );
				_nodes.set( i, _nodes.get( PARENT( i ) ) );
				_nodes.set( PARENT( i ), temp );

				i = PARENT( i );
				if ( i == 1 ) break;
			}
		}

	}

	public MapEntry<TWeight, TValue> remove() {

		if ( _nodes.size() < 2 ) return null;

		MapEntry<TWeight, TValue> ret = new MapEntry<TWeight, TValue>( _nodes.get( 1 ).getWeight(), _nodes.get( 1 ).getValue() );

		if ( _nodes.size() > 2 ) {

			_nodes.set( 1, _nodes.get( _nodes.size() - 1 ) );
			_nodes.remove( _nodes.size() - 1 );
			// _nodes.trimToSize();

			int i = 1;

			while ( _nodes.size() > CHILD_RIGHT( i ) ) {
				/* current node (i) still has two children */

				if ( _nodes.get( CHILD_LEFT( i ) ).getWeight().compareTo( _nodes.get( CHILD_RIGHT( i ) ).getWeight() ) < 0 ) {
					/* left smaller than right */

					if ( _nodes.get( i ).getWeight().compareTo( _nodes.get( CHILD_LEFT( i ) ).getWeight() ) > 0 ) {
						/* parent larger than left. left is smaller than right */

						/* swap */
						HeapNode<TWeight, TValue> temp = _nodes.get( i );
						_nodes.set( i, _nodes.get( CHILD_LEFT( i ) ) );
						_nodes.set( CHILD_LEFT( i ), temp );

						i = CHILD_LEFT( i );

					} else {
						/* parent smaller than children */

						break;
					}
				} else {
					/* right smaller than left */

					if ( _nodes.get( i ).getWeight().compareTo( _nodes.get( CHILD_RIGHT( i ) ).getWeight() ) > 0 ) {
						/* parent larger than right. right is smaller than left */

						/* swap */
						HeapNode<TWeight, TValue> temp = _nodes.get( i );
						_nodes.set( i, _nodes.get( CHILD_RIGHT( i ) ) );
						_nodes.set( CHILD_RIGHT( i ), temp );

						i = CHILD_RIGHT( i );
					} else {
						/* parent smaller than children */

						break;
					}
				}
			}

			if ( _nodes.size() > CHILD_LEFT( i ) ) {
				/*
				 * check left child one last time in case there was never a
				 * right child the loop above won't run
				 */

				if ( _nodes.get( i ).getWeight().compareTo( _nodes.get( CHILD_LEFT( i ) ).getWeight() ) > 0 ) {
					/* parent larger than left. left is smaller than right */

					/* swap */
					HeapNode<TWeight, TValue> temp = _nodes.get( i );
					_nodes.set( i, _nodes.get( CHILD_LEFT( i ) ) );
					_nodes.set( CHILD_LEFT( i ), temp );

					i = CHILD_LEFT( i );

				}

			}
		} else {
			_nodes.remove( 1 );
		}

		return ret;
	}

	public TValue get( TValue value ) {

		for ( HeapNode<TWeight, TValue> hn : _nodes ) {
			if ( hn != null ) if ( hn.getValue().equals( value ) ) { return hn.getValue(); }
		}

		return null;
	}

	public TWeight getWeight( TValue value ) {
		for ( HeapNode<TWeight, TValue> hn : _nodes ) {
			if ( hn != null ) if ( hn.getValue().equals( value ) ) { return hn.getWeight(); }
		}

		return null;
	}

	public boolean isEmpty() {
		return _nodes.size() < 2;
	}

	public String dump() {
		return _nodes.toString();
	}

	public static int PARENT( int index ) {
		return index / 2;
	}

	public static int CHILD_LEFT( int index ) {
		return index * 2;
	}

	public static int CHILD_RIGHT( int index ) {
		return index * 2 + 1;
	}
}
