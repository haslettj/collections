package me.haslett.collections;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

import javax.swing.table.TableColumn;

public class Queue<T> implements java.util.Queue<T> {

	protected QueueNode headNode;
	protected QueueNode tailNode;
	protected int count;

	@Override
	public boolean addAll( Collection<? extends T> arg0 ) {

		return false;
	}

	@Override
	public void clear() {
		headNode = null;
		tailNode = null;
	}

	@Override
	public boolean contains( Object arg0 ) {

		return false;
	}

	@Override
	public boolean containsAll( Collection<?> arg0 ) {

		return false;
	}

	@Override
	public boolean isEmpty() {

		return count == 0;
	}

	@Override
	public Iterator<T> iterator() {

		return null;
	}

	@Override
	public boolean remove( Object arg0 ) {

		return false;
	}

	@Override
	public boolean removeAll( Collection<?> arg0 ) {

		return false;
	}

	@Override
	public boolean retainAll( Collection<?> arg0 ) {

		return false;
	}

	@Override
	public int size() {

		return count;
	}

	@Override
	public Object[] toArray() {

		Object[] array = new Object[count];

		QueueNode<T> currentNode = headNode;
		int i = 0;

		while ( currentNode != null ) {
			array[i++] = currentNode.value;
			currentNode = currentNode.nextNode;
		}

		return array;
	}

	@Override
	public <T> T[] toArray( T[] arg0 ) {

		return (T[]) toArray();
	}

	@Override
	public boolean add( T item ) {

		QueueNode<T> addNode = new QueueNode<T>( item, null );

		if ( tailNode != null ) {
			tailNode.nextNode = addNode;
		} else {
			headNode = addNode;
		}
		tailNode = addNode;
		count++;

		return true;
	}

	@Override
	public T element() {
		T retValue = peek();

		if ( retValue == null ) throw new NoSuchElementException();

		return retValue;
	}

	@Override
	public boolean offer( T item ) {

		return add( item );
	}

	@Override
	public T peek() {

		if ( headNode != null ) return (T) headNode.value;

		return null;
	}

	@Override
	public T poll() {
		T retValue = null;
		if ( headNode != null ) {
			retValue = (T) headNode.value;
			headNode = headNode.nextNode;
			count--;
		}
		if ( headNode == null ) tailNode = null;

		return retValue;
	}

	@Override
	public T remove() {

		T retValue = poll();

		if ( retValue == null ) throw new NoSuchElementException();

		return retValue;
	}

}
