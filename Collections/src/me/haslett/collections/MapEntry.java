package me.haslett.collections;

/**
 * Map of a Key / Value pair.  This class holds a key / value pair as a single object.  It is used as a return value for collections that use a separate key and value.
 * 
 * @author Jim Haslett
 * @since Mar 11, 2013
 * @param <TKey> 	Generic type representing the key.
 * @param <TValue>	Generic type representing the value.
 */
public class MapEntry<TKey, TValue> {

	/**
	 * Constructor.
	 * 
	 * @param key	The key.
	 * @param value	The value.
	 */
	public MapEntry( TKey key, TValue value ) {
		_key = key;
		_value = value;
	}

	TKey _key;
	TValue _value;

	/**
	 * Key accessor.
	 * 
	 * @return	Returns the {@link MapEntry} key.
	 */
	public TKey getKey() {
		return _key;
	}

	/**
	 * Set the key.
	 * 
	 * @param key value to set the {@link MapEntry} key to.
	 */
	public void setKey( TKey key ) {
		_key = key;
	}

	/**
	 * Value accessor.
	 * 
	 * @return Returns the {@link MapEntry} value.
	 */
	public TValue getValue() {
		return _value;
	}

	/**
	 * Set the value.
	 * 
	 * @param value value to set the {@link MapEntry} value to
	 */
	public void setValue( TValue value ) {
		_value = value;
	}

}
