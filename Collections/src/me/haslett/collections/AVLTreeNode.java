package me.haslett.collections;

/**
 * Node used in an AVLTree.
 * 
 * @author Jim Haslett
 * @since Mar 7, 2013
 * @param <TKey>	Generic type representing the key used for sorting.  Must be Comparable.
 * @param <TValue>	Generic type representing the data being stored.

 */
public class AVLTreeNode<TKey extends Comparable<TKey>, TValue> {

	/**
	 * Creates a leaf node with no left or right children.
	 * 
	 * @param Key		Key used for sorting.  Must be Comparable.
	 * @param Value		Data being stored in the Tree.
	 */
	public AVLTreeNode( TKey Key, TValue Value ) {
		this( Key, Value, null, null );
	}

	/**
	 * Creates a node that my have left or right children.  Set left or right no null for no child at that location.
	 * 
	 * @param Key		Key used for sorting.  Must be Comparable.
	 * @param Value		Data being stored in the Tree.
	 * @param Left		Left child node.  null for none.
	 * @param Right		Right child node.  null for none.
	 */
	public AVLTreeNode( TKey Key, TValue Value, AVLTreeNode<TKey, TValue> Left, AVLTreeNode<TKey, TValue> Right ) {
		_key = Key;
		_value = Value;
		_left = Left;
		_right = Right;
		CalculateHeight();
	}

	protected AVLTreeNode<TKey, TValue> _left;
	protected AVLTreeNode<TKey, TValue> _right;
	protected TKey _key;
	protected TValue _value;
	protected int _height;

	/**
	 * Get the value of the TreeNode.
	 * 
	 * @return Value.
	 */
	public TValue getValue() {
		return _value;
	}

	/**
	 * Set the value of the TreeNode.
	 * 
	 * @param value Set the nodes value.
	 */
	public void setValue( TValue value ) {
		_value = value;
	}

	/**
	 * Get the key of the TreeNode.
	 * 
	 * @return Key.
	 */
	public TKey getKey() {
		return _key;
	}

	/**
	 * Get the Left child TreeNode.  The Left child is the "smaller" key.
	 * 
	 * @return Left child node.
	 */
	public AVLTreeNode<TKey, TValue> getLeft() {
		return _left;
	}

	/**
	 * Set the Left child TreeNode.  The Left child is the "smaller" key.
	 * 
	 * @param node	Set the left child node.
	 */
	public void setLeft( AVLTreeNode<TKey, TValue> node ) {
		_left = node;
	}

	/**
	 * Get the Right child TreeNode.  The Right child is the "larger" key.
	 * 
	 * @return Right child node.
	 */
	public AVLTreeNode<TKey, TValue> getRight()

	{
		return _right;
	}

	/**
	 * Set the Right child TreeNode.  The Right child is the "larger" key.

	 * @param node	Set the right child node.
	 */
	public void setRight( AVLTreeNode<TKey, TValue> node ) {
		_right = node;
	}

	/**
	 * @return Height of the node.
	 */
	public int getHeight() {
		return _height;
	}

	/**
	 * Get the balance factor of the current node.  Compares height if right and left child nodes.  Used to determine how balanced this node is.
	 * 
	 * @return	Balance factor of the node.  
	 */
	public int getBalanceFactor() {
		int r, l;
		r = ( _right == null ) ? -1 : _right.getHeight();
		l = ( _left == null ) ? -1 : _left.getHeight(); // was * ? 1 * changed
														// to * ? -1 *
														// 10/10/2006 - empty
														// node should be height
														// -1

		return l - r;
	}

	/**
	 * Get a MapEntry representing teh key / value pair of this TreeNode.
	 * 
	 * @return	MapEntry representing the Key and Value of the node.
	 */
	public MapEntry<TKey, TValue> getMapEntry() {
		return new MapEntry<TKey, TValue>( _key, _value );
	}

	/**
	 * Recalcualtes the height of the node.
	 */
	public void CalculateHeight() {
		int r, l;
		if ( _right == null )
			r = -1;
		else
			r = _right.getHeight();
		if ( _left == null )
			l = -1;
		else
			l = _left.getHeight();
		if ( r > l )
			_height = r + 1;
		else
			_height = l + 1;
	}

}
