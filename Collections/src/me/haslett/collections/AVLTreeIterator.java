package me.haslett.collections;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Stack;

/**
 * Abstract base class for all AVL Tree Iterators.
 * 
 * @author Jim Haslett
 * @since Mar 8, 2013
 * @param <TKey>
 *            Generic type representing the key used for sorting. Must be
 *            Comparable.
 * @param <TValue>
 *            Generic type representing the data being stored.
 */
public abstract class AVLTreeIterator<TKey extends Comparable<TKey>, TValue> implements Iterator<MapEntry<TKey, TValue>> {

	protected enum StatusEnum {
		OK, BeforeFirst, AfterLast, Invalid
	}

	/**
	 * Constructor.
	 * 
	 * @param Root
	 *            AVLTreeNode where the iteration will start.
	 * @throws Exception
	 */
	public AVLTreeIterator( AVLTreeNode<TKey, TValue> Root ) throws Exception {
		_current = null;
		_status = StatusEnum.BeforeFirst;
		_stack = new Stack<AVLTreeNode<TKey, TValue>>();
		_root = Root;

		MoveNext();
	}

	protected AVLTreeNode<TKey, TValue> _current;
	protected StatusEnum _status;
	protected Stack<AVLTreeNode<TKey, TValue>> _stack;
	protected AVLTreeNode<TKey, TValue> _root;

	/**
	 * Moves the current pointer to the next element. The next element is
	 * determined by the traversal method.
	 * 
	 * @return True if MoveNext was successful and there was a valid element to
	 *         move to, otherwise false.
	 * @throws Exception
	 */
	protected abstract boolean MoveNext() throws Exception;

	/**
	 * Returns the current element in the iteration.
	 * 
	 * @return The TValue element at the current pointer location.
	 * @throws Exception
	 */
	protected MapEntry<TKey, TValue> getCurrent() throws Exception

	{
		if ( _status == StatusEnum.OK || _status == StatusEnum.Invalid ) {
			return new MapEntry<TKey, TValue>( _current.getKey(), _current.getValue() );
		} else if ( _status == StatusEnum.BeforeFirst )
			throw ( new Exception( "! Before first element of AVL Search Tree, Call MoveNext first !" ) );
		else if ( _status == StatusEnum.AfterLast )
			throw ( new Exception( "! After Last element of AVL Search Tree !" ) );
		else
			throw ( new Exception( "! Unknown Status during iteration !" ) );
	}

	@Override
	public boolean hasNext() {
		return _status == StatusEnum.OK;
	}

	@Override
	public MapEntry<TKey, TValue> next() throws NoSuchElementException {
		MapEntry<TKey, TValue> ret = null;
		try {
			ret = getCurrent();
			MoveNext();
		} catch ( Exception e ) {
			throw new NoSuchElementException( e.getMessage() );
		}

		return ret;
	}

	@Override
	public void remove() throws UnsupportedOperationException {
		throw ( new UnsupportedOperationException( "This Iterator does not support removing elements!" ) );
	}

}
