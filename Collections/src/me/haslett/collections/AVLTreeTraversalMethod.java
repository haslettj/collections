package me.haslett.collections;

/**
 * enum used to determine the order of Iteration traversal of an AVLTree.
 * 
 * @author Jim Haslett
 * @since Mar 8, 2013
 */
public enum AVLTreeTraversalMethod {
	/**
	 * Iterates an AVL tree in natural order represented by the Key.
	 */
	InOrder,
	/**
	 * Iterates an AVL tree in reverse order represented by the Key.
	 */
	ReverseOrder,
	/**
	 * Iterates an AVL tree in top down order. This iterator traverses the
	 * AVLTree in special way that is useful for serializing or saving the tree
	 * for the purpose of reloading another tree. Inserting the elements into a
	 * tree in the order they are iterated here is the fastest way to load the
	 * tree without necessary and costly sorting.
	 */
	TopDown
}
