package me.haslett;

import me.haslett.collections.AVLTree;
import me.haslett.collections.AVLTreeTraversalMethod;
import me.haslett.collections.MapEntry;

public class Tester {

	public static void main( String[] args ) {
		AVLTree<Integer, String> tree = new AVLTree<Integer, String>();

		try {
			tree.Add( 1, "one" );
			tree.Add( 2, "two" );
			tree.Add( 3, "three" );
			tree.Add( 4, "four" );
			tree.Add( 6, "six" );
			tree.Add( 5, "five" );
		} catch ( Exception e ) {
			System.out.println( e.getMessage() );
		}

	

		for ( MapEntry<Integer, String> mapEntry : tree ) {

			System.out.println( mapEntry.getKey() + " - " + mapEntry.getValue() );
		}

		System.out.println( tree.getCount() );
		System.out.println( tree.getMinKey() );
		System.out.println( tree.getMaxKey() );

		System.out.println("Removed " + tree.Remove( 3 ).getValue());

		System.out.println("Removed " + tree.Remove( 1 ).getKey());

		tree.setTraversalMethod( AVLTreeTraversalMethod.TopDown );
		
		for ( MapEntry<Integer, String> mapEntry : tree ) {

			System.out.println( mapEntry.getKey() + " - " + mapEntry.getValue() );
		}
		
		System.out.println(tree.get(5).getValue());
		
	}
}
